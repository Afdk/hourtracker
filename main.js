const fs = require('fs');
let csv = fs.readFileSync('./'+process.argv[2], 'utf8')
let sickDays = process.argv[3] || 0
let csvArr = csv.split('\n')
let cant = 1
let line
let days = new Set()
let projects = new Map()
let tasks = new Map()
let workDay = 6
let lunchTime = 0.5

let totalHours = 0
console.log(csvArr.length)
for (k = 1; k< csvArr.length -1 ; k++) {
  //Ignore Header
  line = csvArr[k].split(';') //Creo línea con datos en distintas entries
  // line[2] = proyecto
  // line[5] = Tarea
  // line[12] = Fecha Inicio
  // line[15] =  Duración
  // line[16] =  Horas
  
  if (line[12]) { // Evito datos mal cargados
    days.add(line[12].slice(1,11)) //Agrego día a la lista
  }
  
  if (line[16]){
    
    if (line[2]) { // Evito datos mal cargados
      let key = line[2].slice(1,line[2].length -1)
      let hours = parseFloat(line[16].slice(1,line[16].length -1).replace(',','.'))
      if (projects.get(key)) {
        hours += parseFloat(projects.get(key))
      }
      projects.set(key,hours.toFixed(2)) //Agrego día a la lista


      if (line[5]) { // Evito datos mal cargados
        let key = line[2].slice(1,line[2].length -1) +  ' - ' + line[5].slice(1,line[5].length -1)
        let hours = parseFloat(line[16].slice(1,line[16].length -1).replace(',','.'))
        totalHours += hours // cargo horas a horas totales del archivo

        if (tasks.get(key)) {
          hours += parseFloat(tasks.get(key))
        }
        tasks.set(key,hours.toFixed(2)) //Agrego día a la lista
      }
    }
  }
  cant++
}


line = csvArr[1].split(';')

console.log(days)
console.log(days.size)


console.log(projects)
console.log(projects.size)

console.log(tasks)
console.log(tasks.size)

console.log('Days worked: ' + days.size)
let estimatedDiff = (days.size*workDay - days.size*lunchTime)
console.log('Estimated work hours in period: ' + days.size*workDay)
console.log('Estimated Lunch Hours: ' + (days.size*lunchTime))
console.log('Estimated work hours in period (without lunch): ' + (days.size*workDay - days.size*lunchTime))

let doneDiff = (totalHours.toFixed(2) - tasks.get('Kreitech - Almorzando') + workDay*sickDays)
console.log('Hours worked in period: ' + totalHours.toFixed(2))
console.log('Lunch Hours taken: ' + (tasks.get('Kreitech - Almorzando')))
console.log('Sick Days (added to worked hours): ' + workDay*sickDays )
console.log('Hours worked in period (without lunch): ' + (totalHours.toFixed(2) - tasks.get('Kreitech - Almorzando')).toFixed(2) )


if (doneDiff > estimatedDiff) {
  console.log('Extra Hours : ' + (doneDiff - estimatedDiff).toFixed(2))
} else {
  console.log('Trailing Hours : ' + (doneDiff - estimatedDiff).toFixed(2))
}
/*
-;-;Proyecto;-;-;Tarea;-;-;-;-;-;-;Fecha de inicio;-;-;Duración;Horas;-;-;-;-;-
*/
 
