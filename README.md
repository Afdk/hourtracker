# hourTracker #
This tiny tiny app serves one purpose and it is helping those who use TrackingTime and skip lunch.

### How do I get set up? ###
1. Clone it
2. Export your TrackingTime Report on .csv file with `;` as separators
3. Rename it to "hours.csv" and copy it into the same folder (no worries, it is .gitignored)
4. Run `node main.js hours.csv`

Then, you will probably have to do a bunch of const settings yourselves, I didnt have time to make a little interface for it.

### Contact ###

email at: 
`afriss@kreitech.io`
